<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductoRequest;
use App\Models\Producto;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $productos = Producto::all();

        return response()->json($productos,200);
        
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductoRequest $request)
    {
        $datos = $request->only([
            'codigo',
            'descripcion',
            'existencia',
            'precio'
            ]);
        
        $producto = Producto::create($datos);

        return response()->json($producto,200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Producto::findOrFail($id);
        return response()->json($produto,200);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductoRequest $request, $id)
    {

        $producto = Producto::findOrFail($id);

        $datos = $request->only(['codigo','descripcion','existencia','precio']);

        $producto->codigo = $datos['codigo'];
        $producto->descripcion = $datos['descripcion'];
        $producto->existencia = $datos['existencia'];
        $producto->precio = $datos['precio'];

        $producto->update($datos);

        return response()->json($producto,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turno = Producto::findOrFail($id);
        $turno->delete();
        return response()->json(['result'=>'deleted'],200);
    }
}
