<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Log;
abstract class Request extends FormRequest
{
    public function response(array $errors)
    {
        $err = [];
        foreach ($errors as $error) {
            $err[] = $error[0];
        }

        Log::info($err);

        return response()->json(array('errors'=>$err), 422);
    }
}
