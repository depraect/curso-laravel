<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Log;


class ProductoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        $rules = [];
        /*
        *   Actualizacion
        */

        if($this->method()=='PUT'){
            $id = $this->input('id');
            Log::info($this->all());
            $rules = [
                'codigo'=>"required|max:10|unique:productos,codigo,$id,id",
                'descripcion'=>'required|string|max:255',
                'existencia'=>'required|integer',
                'precio'=>'required|numeric'
            ];

            Log::info($rules);
        }

        /*
        *
        *   nuevo registro 
        */

        else{
            $rules = [
                'codigo'=>"required|max:10|unique:productos,codigo",
                'descripcion'=>'required|string|max:255',
                'existencia'=>'required|integer',
                'precio'=>'required|numeric'
            ];
        }



        return $rules;
        
    }
}
