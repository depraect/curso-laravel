<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductoTest extends TestCase
{


    public function testCreateProduct(){

        $datos = [
            'codigo'=>'MACBOOK',
            'descripcion'=> 'MacBook Pro Retina Display sin usbs',
            'existencia'=>10,
            'precio'=>38000.00
        ];

        $this->post('/api/productos',$datos)
            ->seeJsonStructure([
                'id', 
                'codigo', 
                'descripcion', 
                'existencia',
                'precio']);
    }
    
    
    public function testExample()
    {
        $this->get('/api/productos')
     		->seeJsonStructure(
     			['*' => [
     			'id', 
     			'codigo', 
     			'descripcion', 
     			'existencia',
     			'precio']]
         	);
    }

    public function testShow(){
    	$this->get('/api/productos/1')
    		->seeJsonStructure([
    			'id', 
     			'codigo', 
     			'descripcion', 
     			'existencia',
     			'precio']
			);
    }

    

    public function testCreateFailProduct(){

        $datos = [
            'codigo'=>'MACBOOK',
            'descripcion'=> 'MacBook Pro Retina Display sin usbs',
            'existencia'=>10,
            'precio'=>38000.00
        ];

        $this->post('/api/productos',$datos)
            ->assertResponseStatus(422);
    }

    public function testUpdateProducto(){

        $datos = [
            'id'=>1,
            'codigo'=>'MACBOOK',
            'descripcion'=> 'MacBook Pro Retina Display 2017',
            'existencia'=>10,
            'precio'=>36000.00
        ];

        $this->put('/api/productos/1',$datos)
            ->seeJsonStructure([
                'id', 
                'codigo', 
                'descripcion', 
                'existencia',
                'precio']
            );
    }


    public function testDeleteProducto(){

        $this->delete('/api/productos/1')
            ->assertResponseOk()
            ->seeJson(['result'=>'deleted']);
    }
    


}
